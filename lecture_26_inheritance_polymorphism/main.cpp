﻿#include <iostream>
#include <vector>

struct Pixel
{
	unsigned char r, g, b;
};

// Базовый класс, родительский класс
class Images
{
public:
	Images(int width, int height)
	{
		m_width = width;
		m_height = height;
		std::cout << "Base class: CONSTRUCTOR" << std::endl;

		m_pixels = new Pixel * [width];
	}

	virtual ~Images()
	{
		std::cout << "Base class: DESTRUCTOR" << std::endl;

		delete[] m_pixels;
	}

	void Filter()
	{
		std::cout << "Base class: FILTER" << std::endl;
	}

	void Rotate(double angle)
	{
		std::cout << "Base class: ROTATE" << std::endl;
	}

	virtual void Open(const std::string& filename) = 0;
	virtual void Save(const std::string& filename) = 0;

protected:
	Pixel** m_pixels;
	int m_width;
	int m_height;
};

// Потомок
class BMP : public Images
{
public:
	BMP(int width, int height) : Images(width, height)
	{
		std::cout << "BMP class: constructor" << std::endl;
		m_mas = new int[width];
		m_pixels = new Pixel * [width];
	}

	virtual ~BMP()
	{
		std::cout << "BMP class: destructor" << std::endl;
		delete[] m_mas;
	}

	void Open(const std::string& filename) override
	{
		std::cout << "BMP class: Open" << std::endl;
	}

	void Save(const std::string& filename) override
	{
		std::cout << "BMP class: Save" << std::endl;
	}

private:
	int* m_mas;
};

// Потомок
class JPG : public Images
{
public:
	JPG(int width, int height) : Images(width, height)
	{
		std::cout << "JPG class: constructor" << std::endl;
	}

	virtual ~JPG()
	{
		std::cout << "JPG class: destructor" << std::endl;
	}

	void Open(const std::string& filename) override
	{
		std::cout << "JPG class: Open" << std::endl;
	}

	void Save(const std::string& filename) override
	{
		std::cout << "JPG class: Save" << std::endl;
	}
};

std::string Extension(const std::string& s)
{
	int index = s.rfind('.');
	std::string r = s.substr(index, s.length() - index);
	return r;
}

Images* imagesFactory(const std::string& filename)
{
	std::string extension = Extension(filename);

	Images* image = nullptr;

	if (extension == ".bmp")
	{
		image = new BMP(100, 200);
		image->Open(filename);
	}
	else if (extension == ".jpg")
	{
		image = new JPG(20, 10);
		image->Open(filename);
	}
	else if (extension == ".png")
	{
		image = new PNG(20, 10);
		image->Open(filename);
	}

	return image;
}

int main()
{
	std::vector<Images*> images;

	Images* image = imagesFactory("filename.bmp");
	images.push_back(image);

	image = imagesFactory("filename.jpg");
	images.push_back(image);

	for (int i = 0; i < images.size(); i++)
		images[i]->Filter();

	for (int i = 0; i < images.size(); i++)
		delete images[i];


	return 0;
}