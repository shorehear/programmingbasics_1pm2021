#include <iostream>
#include <chrono>
#include <ctime>
#include <thread>

using std::chrono_literals;
std::this_thread::sleep_for(1s);

void Change(int& n)
{
	n = 1234;
	_sleep(10);
}

void Test()
{
	/*int n;
	std::cin >> n;
	int* mas = new int[n];

	for (int i = 0; i < n; i++)
		std::cin >> mas[i];

	int sum = 0;
	for (int i = 0; i < n; i++)
		sum += mas[i];

	std::cout << sum << std::endl;

	delete[] mas;*/

	/*for (;;)
	{
		int* mas = new int[100000000];
		std::this_thread::sleep_for(std::chrono::seconds(3));	// приостановление выполнения программы на 1 сек
		delete[] mas;
		std::this_thread::sleep_for(std::chrono::seconds(3));
	}*/

	/*int n = 3;
	int m = 3;

	int** table = new int* [n];
	for (int i = 0; i < n; i++)
		table[i] = new int[m];

	table[0][0] = 10;

	for (int i = 0; i < n; i++)
		delete[] table[i];
	delete[] table;*/

	int mas[10] = { 1,2,3,4,5,6,7,8,9,10 };
	std::cout << mas[4] << std::endl;
	std::cout << *(mas + 4) << std::endl;
	std::cout << mas << std::endl;
	std::cout << (mas+1) << std::endl;
}

// Передача массива в функцию
void sort(int** mas)
{

}

bool Func()
{
	int* mas = new int[1000];

	int x;
	std::cin >> x;
	if (x > 10)
	{
		// Программа упала
	}

	delete[] mas;
	return false;
	
}

void Task()
{
	/*// 3 различных типа элементов. Каждого из типов может быть до 10^6
	// Суммарно всех 3 элементов может быть не более 10^6
	int mas1[1000000]; // 4мб
	double mas2[1000000]; // 8мб
	char mas3[1000000]; // 1мб

	int n, m, k;

	int* mas1 = new int[n];
	double* mas2 = new double[m];	
	char* mas3 = new char[k];		// 1мб

	delete[] mas1;
	delete[] mas2;
	delete[] mas3;*/


}

int main()
{
	// Ссылки
	int n = 1000;
	std::cout << &n << std::endl;

	// 1. Передача параметров в функции
	std::cout << n << std::endl;
	Change(n);
	std::cout << n << std::endl;

	// 2. Псевдоним
	int sumOfDigitsOfPrimeNumber = 34;
	int& sum = sumOfDigitsOfPrimeNumber;

	std::cout << sumOfDigitsOfPrimeNumber << std::endl;
	std::cout << sum << std::endl;

	// Указатели
	int* pointer = &n;
	std::cout << pointer << std::endl;
	std::cout << *pointer << std::endl;
	std::cout << sizeof(pointer) << std::endl;
	std::cout << sizeof(*pointer) << std::endl;
	std::cout << &pointer << std::endl;

	// Область памяти - стек
	// 1. Фиксированный размер
	// 2. Быстрота
	// 3. Контроль со стороны компилятора
	int x;
	int mas[1000];	// "на стеке"

	// Область памяти - куча (Heap = хип)
	// 1. Произвольный размер
	// 2. Чуть медленнее за счет выделения памяти
	// 3. Отсустствие контроля
	int* y = new int;
	*y = 10;
	(*y)++;
	std::cout << *y << std::endl;

	delete y;
	// Memery leak = утечка памяти

	//Test();

	// Указатель, который никуда не указывает
	int* p = nullptr;
	if (p != nullptr)
	{
		delete p;
	}
	std::cout << p << std::endl;

	return 0;
}