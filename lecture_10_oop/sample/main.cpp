#include <iostream>
#include <circle.hpp>

int main()
{
	const int N = 5;
	// Создать 5 кругов и вывести их площадь
	mt::Circle circles[N];
	for (int i = 0; i < N; i++)
	{
		int R;
		mt::Point M;
		std::cin >> R >> M.x >> M.y;
		circles[i].SetM(M);
		circles[i].SetR(R);
	}

	for (int i = 0; i < N; i++)
		std::cout << "Square of i circle = " << circles[i].Square() << std::endl;


	/*Point p;
	p.x = 0;
	p.y = 0;

	Circle circle(p, 5);

	std::cout << "R = " << circle.GetR() << std::endl;

	circle.SetR(-10);

	std::cout << "R = " << circle.GetR() << std::endl;

	std::cout << "Square is " << circle.Square() << std::endl;*/

	return 0;
}


/*// Хранение данных
struct Point
{
	int x;
	int y;
	int z;
};

struct Rectangle
{
	Point p1, p2;
};

double Distance(Point p1, Point p2)
{
	return sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y));
}

Point Mediana(Point p1, Point p2)
{
	Point p;
	p.x = (p1.x + p2.x) / 2;
	p.y = (p1.y + p2.y) / 2;
	return p;
}

int main()
{
	Point mas[10];
	mas[0].x = 1;

	Point p1;
	p1.x = 0;
	p1.y = 0;

	Point p2;
	p2.x = 10;
	p2.y = 20;

	std::cout << p1.x << " " << p1.y << std::endl;
	std::cout << p2.x << " " << p2.y << std::endl;
	std::cout << Distance(p1, p2) << std::endl;

	Point p3 = Mediana(p1, p2);
	std::cout << p3.x << " " << p3.y << std::endl;

	Rectangle rect;
	rect.p1.x = 0;
	rect.p1.y = 0;
	rect.p2.x = 10;
	rect.p2.y = 10;

	return 0;
}*/